# group2
Normalizador de Archivos Obstétricos
Este proyecto tiene como objetivo desarrollar un sistema de normalización de archivos obstétricos. El sistema está escrito en Python y PHP y es compatible con archivos de texto plano.
Funcionalidades
    • Normalización de archivos de texto plano.
    • Búsqueda de información obstétrica a través de un buscador.
    • Función de login segura para acceder al sistema.
    • Extracción de información obstétrica y organización en tablas utilizando Python.
    • Establecimiento de relaciones entre tablas para asegurar una correcta organización de la información.
    • Guardado de información en archivos planos (.dat) para asegurar su accesibilidad y seguridad.
Instalación y Uso
Para instalar y utilizar este sistema, sigue los siguientes pasos:
    1. Clona el repositorio en tu máquina local: git clone https://gitlab.com/tu-usuario/normalizador-obstetrico.git
    2. Accede al directorio del proyecto: cd normalizador-obstetrico
    3. Ejecuta el script de instalación: ./install.sh
    4. Inicia el servidor: php -S localhost:8000
    5. Accede al sistema a través de tu navegador web: http://localhost:8000
Contribuciones
Si deseas contribuir al desarrollo de este proyecto, por favor sigue estos pasos:
    1. Haz un fork del repositorio: https://gitlab.com/tu-usuario/normalizador-obstetrico.git
    2. Crea una nueva rama para tus cambios: git checkout -b nueva-funcionalidad
    3. Realiza tus cambios y realiza commit: git commit -m "Agrega nueva funcionalidad"
    4. Realiza un push a tu rama: git push origin nueva-funcionalidad
    5. Crea un merge request para solicitar la integración de tus cambios al repositorio principal.

Alargar el buscador 
maximizar el boton de buscar y darle estilos 

